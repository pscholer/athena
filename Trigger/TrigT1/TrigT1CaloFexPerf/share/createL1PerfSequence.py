# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration

from TrigT1CaloFexPerf.L1PerfSequence import setupRun3L1CaloPerfSequence
setupRun3L1CaloPerfSequence()
