/*
  Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
*/

#include "MuonRDO/RpcBis78_TrigRawDataContainer.h"

Muon::RpcBis78_TrigRawDataContainer::RpcBis78_TrigRawDataContainer() :
  DataVector<Muon::RpcBis78_TrigRawData>()
{

}


